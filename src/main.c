#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>

#include "mem.h"

void test1(char const* test_name) {
    printf("%s\n", test_name);
    void* heap = heap_init(500);
    debug_heap(stdout, heap);
    _malloc(1);
    _malloc(400);
    debug_heap(stdout, heap);
}

void test2(char const* test_name) {
    printf("%s\n", test_name);
    void* heap = heap_init(500);
    void *ptr = _malloc(200);
    _malloc(200);
    debug_heap(stdout, heap);
    _free(ptr);
    debug_heap(stdout, heap);
}

void test3(char const* test_name) {
    printf("%s\n", test_name);
    void* heap = heap_init(500);
    void* ptr1 = _malloc(100);
    _malloc(100);
    void* ptr2 = _malloc(100);
    debug_heap(stdout, heap);
    _free(ptr1);
    _free(ptr2);
    debug_heap(stdout, heap);
}

void test4(char const* test_name) {
    printf("%s\n", test_name);
    void* heap = heap_init(10000);
    _malloc(5000);
    debug_heap(stdout, heap);
    _malloc(10000);
    debug_heap(stdout, heap);
}

static void* pages_copy(void const* addr, size_t length, int additional_flags) {
    return mmap((void*) addr, length, PROT_READ | PROT_WRITE, additional_flags, 0, 0);
}

void test5(char const* test_name) {
    printf("%s\n", test_name);
    void* heap = heap_init(8096);
    _malloc(100);
    debug_heap(stdout, heap);
    pages_copy(heap, 10000, 0);
    _malloc(10000);
    debug_heap(stdout, heap);
}

int main() {
    test1("Usual allocation");
    test2("Free 1 block");
    test3("Free 2 block");
    test4("New region");
    test5("New region in a new place");
}
